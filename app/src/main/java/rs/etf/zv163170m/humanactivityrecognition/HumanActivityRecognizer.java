package rs.etf.zv163170m.humanactivityrecognition;

import android.content.Context;
import android.content.res.AssetManager;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by vzbiljic on 13.8.2017.
 */

public class HumanActivityRecognizer {
    static {
        System.loadLibrary("tensorflow_inference");
    }

    private static final String TAG = "HumanActivityRecognizer";

    private static final long SAMPLE_PERIOD_MILLIS = 50;
    private static final long WAIT_TIME = 100;
    private static final String MODEL_FILE = "file:///android_asset/my_model.pb";
    private static final String INPUT_NODE = "lstm_1_input";
    private static final long[] INPUT_SIZE = new long[]{1, 200, 3};
    private static final int WINDOW_SIZE = 200;
    private static final int OUTPUT_SIZE = 6;

    private IAccelerometerDataSource mDataSource;
    private List<Float> X_data = new ArrayList<Float>(), Y_data = new ArrayList<Float>(), Z_data = new ArrayList<Float>();
    private boolean inputThreadRunning;
    private boolean outputThreadRunning;
    private TensorFlowInferenceInterface predictorInterface;
    private AssetManager assetManager;
    private IHumanActivityChangedListener callback;
    private HumanActivityState state;
    private Semaphore mutex = new Semaphore(1);

    private Thread inputThreadObject = new Thread() {
        @Override
        public void run() {
            inputThread();
        }
    };
    private Thread outputThreadObject = new Thread() {
        @Override
        public void run() {
            outputThread();
        }
    };

    private float[] getProbability(float[] input_data) {
        float[] result = new float[OUTPUT_SIZE];
        predictorInterface.feed(INPUT_NODE, input_data, new long[]{ 1, 200, 3});
        predictorInterface.run(new String[]{"activation_1/Softmax"});
        predictorInterface.fetch("activation_1/Softmax", result);

        return result;
    }


    private void inputThread(){
        while (inputThreadRunning){
            AccelerometerData currentReading = mDataSource.getCurrentReading();

            try {
                mutex.acquire();

                //remove fist element to keep window size
                if(X_data.size() == WINDOW_SIZE ){
                    X_data.remove(0);
                    Y_data.remove(0);
                    Z_data.remove(0);
                }

                X_data.add(currentReading.getX());
                Y_data.add(currentReading.getY());
                Z_data.add(currentReading.getZ());

                mutex.release();

                Thread.sleep(SAMPLE_PERIOD_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void outputThread(){
        while (outputThreadRunning){
            try {
                if(X_data.size() == WINDOW_SIZE ) {
                    mutex.acquire();

                    //load current data
                    List<Float> list = new ArrayList<>();

                    list.addAll(X_data);
                    list.addAll(Y_data);
                    list.addAll(Z_data);

                    mutex.release();

                    //calculate predictions
                    float[] inputs = toFloatArray(list);
                    float[] outputs = getProbability(inputs);

                    float max = 0;
                    int maxIndex = 0;
                    for(int i=0 ; i < outputs.length; i++){
                        if(outputs[i] > max){
                            max = outputs[i];
                            maxIndex = i;
                        }
                    }

                    //create new state
                    HumanActivityState newState = HumanActivityState.getIntValue(maxIndex);

                    if(state != newState){
                        if(null != callback)
                            callback.onStateChanged(newState);

                        state = newState;
                    }

                }

                Thread.sleep(WAIT_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private float[] toFloatArray(List<Float> list) {
        int i = 0;
        float[] array = new float[list.size()];

        for (Float f : list) {
            array[i++] = (f != null ? f : Float.NaN);
        }
        return array;
    }

    /***
     *  Recommended way of instantiation.
     * @param context Context needed by TensorFlow engine
     * @param callback Reference to listener, waiting on state change
     */
    public HumanActivityRecognizer(final Context context, IHumanActivityChangedListener callback) {
        this(context,callback,new GMSAccelerometerDataSource(context));
    }

    /***
     * Use this constructor only if you have custom data source, and you don't want to use built in
     * Accelerometer from Android.
     *
     * @param context Context needed by TensorFlow engine
     * @param callback Reference to listener, waiting on state change
     * @param dataSource Custom data source.
     */
    public HumanActivityRecognizer(final Context context, IHumanActivityChangedListener callback, IAccelerometerDataSource dataSource) {
        this.assetManager = context.getAssets();
        mDataSource = dataSource;
        predictorInterface = new TensorFlowInferenceInterface(assetManager, MODEL_FILE);

        this.callback = callback;

        //start input and output thread
        inputThreadRunning = true;
        outputThreadRunning = true;
        inputThreadObject.start();
        outputThreadObject.start();

    }

    /***
     * Use to pool current state from Recognizer. Use if you don't need Listener to retrive current state.
     * @return Current state
     */
    public HumanActivityState getCurrentState(){
        return state;
    }

    /***
     * Should be called from onDestroy method in MainActivity to stop recognizer threads.
     */
    public void stop(){
        inputThreadRunning = false;
        outputThreadRunning = false;
    }



}

