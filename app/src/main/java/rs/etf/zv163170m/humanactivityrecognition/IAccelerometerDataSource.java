package rs.etf.zv163170m.humanactivityrecognition;

/**
 * Created by vzbiljic on 15.8.2017.
 */

public interface IAccelerometerDataSource {
    //get current accelerometer X value
    AccelerometerData getCurrentReading();
}
