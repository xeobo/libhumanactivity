package rs.etf.zv163170m.humanactivityrecognition;

/**
 * Created by vzbiljic on 19.8.2017.
 */

public interface IHumanActivityChangedListener {

    void onStateChanged(HumanActivityState newState);
}
