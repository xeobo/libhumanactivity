package rs.etf.zv163170m.humanactivityrecognition;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.concurrent.Semaphore;

/**
 * Created by vzbiljic on 15.8.2017.
 */

public class GMSAccelerometerDataSource implements IAccelerometerDataSource, SensorEventListener {
    private Context context;
    private Sensor mySensor;
    private Semaphore semaphore = new Semaphore(1);
    private float x,y,z;

    public GMSAccelerometerDataSource(Context context){
        this.context = context;

        SensorManager mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public AccelerometerData getCurrentReading() {
        AccelerometerData data = null;
        try {
            semaphore.acquire();

            data = new AccelerometerData(x, y, z);

            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            semaphore.acquire();

            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
