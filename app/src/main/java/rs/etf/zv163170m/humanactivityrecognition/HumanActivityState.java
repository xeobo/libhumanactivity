package rs.etf.zv163170m.humanactivityrecognition;

/**
 * Created by vzbiljic on 19.8.2017.
 */

public enum HumanActivityState {
    DOWNSTAIRS,
    JOGGING,
    SITTING,
    STANDING,
    UPSTAIRS,
    WALKING,
    IDLE;


    public static HumanActivityState getIntValue(int value){
        switch (value){
            case 0:
                return DOWNSTAIRS;
            case 1:
                return JOGGING;
            case 2:
                return SITTING;
            case 3:
                return STANDING;
            case 4:
                return UPSTAIRS;
            case 5:
                return WALKING;
            default:
                return IDLE;

        }
    }

    @Override
    public String toString() {
        switch (this){
            case DOWNSTAIRS:
                return "DOWNSTAIRS";
            case JOGGING:
                return "JOGGING";
            case SITTING:
                return "SITTING";
            case STANDING:
                return "STANDING";
            case UPSTAIRS:
                return "UPSTAIRS";
            case WALKING:
                return "WALKING";
            default:
                return "IDLE";
        }
    }
}
