package rs.etf.zv163170m.humanactivityrecognition;

/**
 * Created by vzbiljic on 15.8.2017.
 */

public class AccelerometerData {
    float x;
    float y;
    float z;

    public AccelerometerData(float x, float y, float z) {
        this.y = y;
        this.z = z;
        this.x = x;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
}
